let personnages = {
    'ruby': {
        'name': 'Ruby',
        'pv': 110,
        'maxPv': 110,
        'heal': 30,
        'power': 35,
        'img': "assets/image/ruby.png",
    },
    'rem': {
        'name': 'Rem',
        'pv': 90,
        'maxPv': 90,
        'heal': 30,
        'power': 20,
        'img': "assets/image/rem.png",
    },
    'serena': {
        'name': 'Serena',
        'pv': 110,
        'maxPv': 110,
        'heal': 35,
        'power': 25,
        'img': "assets/image/serena.png",
    },
    'elzabeth': {
        'name': 'Elzabeth',
        'pv': 100,
        'maxPv': 100,
        'heal': 25,
        'power': 30,
        'img': "assets/image/elizabeth.png",
    },
    'suicune': {
        'name': 'Suicune',
        'pv': 160,
        'maxPv': 160,
        'heal': 30,
        'power': 40,
        'img': "assets/image/suicune.png",
    },
    'hellHunter': {
        'name': 'HellHunter',
        'pv': 160,
        'maxPv': 160,
        'heal': 20,
        'power': 40,
        'img': "assets/image/hellHunter.png",
    },
    'batman': {
        'name': 'Batman',
        'pv': 110,
        'maxPv': 110,
        'heal': 30,
        'power': 35,
        'img': "assets/image/batman.png",
    },
    'raven': {
        'name': 'Raven',
        'pv': 90,
        'maxPv': 90,
        'heal': 30,
        'power': 20,
        'img': "assets/image/raven.png",
    },
    'yami': {
        'name': 'Yami',
        'pv': 110,
        'maxPv': 110,
        'heal': 35,
        'power': 25,
        'img': "assets/image/yami.png",
    },
    'jillValentin': {
        'name': 'Jill Valentin',
        'pv': 100,
        'maxPv': 100,
        'heal': 25,
        'power': 30,
        'img': "assets/image/jillValentin.png",
    },
    'sephiroth': {
        'name': 'Sephiroth',
        'pv': 160,
        'maxPv': 160,
        'heal': 30,
        'power': 40,
        'img': "assets/image/sephiroth.png",
    },
    'kenKaneki': {
        'name': 'Ken Kaneki',
        'pv': 160,
        'maxPv': 160,
        'heal': 20,
        'power': 40,
        'img': "assets/image/kenKaneki.png",
    },
    'card13': {
        'name': 'card1',
        'pv': 110,
        'maxPv': 110,
        'heal': 30,
        'power': 35,
        'img': "assets/image/ruby.png",
    },
    'card14': {
        'name': 'card2',
        'pv': 90,
        'maxPv': 90,
        'heal': 30,
        'power': 20,
        'img': "assets/image/ruby.png",
    },
    'card15': {
        'name': 'card3',
        'pv': 110,
        'maxPv': 110,
        'heal': 35,
        'power': 25,
        'img': "assets/image/ruby.png",
    },
    'card16': {
        'name': 'card4',
        'pv': 100,
        'maxPv': 100,
        'heal': 25,
        'power': 30,
        'img': "assets/image/ruby.png",
    },
    'card17': {
        'name': 'card5',
        'pv': 160,
        'maxPv': 160,
        'heal': 30,
        'power': 40,
        'img': "assets/image/ruby.png",
    },
    'card18': {
        'name': 'card6',
        'pv': 160,
        'maxPv': 160,
        'heal': 20,
        'power': 40,
        'img': "assets/image/ruby.png",
    },
    'card19': {
        'name': 'card1',
        'pv': 110,
        'maxPv': 110,
        'heal': 30,
        'power': 35,
        'img': "assets/image/ruby.png",
    },
    'card20': {
        'name': 'card2',
        'pv': 90,
        'maxPv': 90,
        'heal': 30,
        'power': 20,
        'img': "assets/image/ruby.png",
    },
    'card21': {
        'name': 'card3',
        'pv': 110,
        'maxPv': 110,
        'heal': 35,
        'power': 25,
        'img': "assets/image/ruby.png",
    },
    'card22': {
        'name': 'card4',
        'pv': 100,
        'maxPv': 100,
        'heal': 25,
        'power': 30,
        'img': "assets/image/ruby.png",
    },
    'card23': {
        'name': 'card5',
        'pv': 160,
        'maxPv': 160,
        'heal': 30,
        'power': 40,
        'img': "assets/image/ruby.png",
    },
    'card24': {
        'name': 'card6',
        'pv': 160,
        'maxPv': 160,
        'heal': 20,
        'power': 40,
        'img': "assets/image/ruby.png",
    },
}

//pagination
let firstBtn = document.getElementById('firstBtn')
let prevBtn = document.getElementById('prevBtn')
let currentPage = document.getElementById('actualPage')
let numberOfPage = document.getElementById('numberOfPage')
let nextBtn = document.getElementById('nextBtn')
let lastBtn = document.getElementById('lastBtn')
let cardImg = document.querySelectorAll("#cardJs img")
let containerCard = document.getElementById('cardJs')


// todolist
let chooseDeck = document.querySelector(".iconDeck")
let containerLocation = document.querySelector("#containerCard")

const keys = Object.keys(personnages);
let i = 0
let firstIndex = 0
let cardsByPage = 8
let pageActive = 1

let PageStart = 0
let PageEnd = cardsByPage
document.body.onload = generateUserRow(firstIndex, cardsByPage)
numberOfPage.innerHTML = keys.length / cardsByPage
numberOfPage = keys.length / cardsByPage

// keys.forEach(element => {
//     i++
//     img = document.createElement('img')
//     img.classList.add(i)
//     img.src = personnages[element].img
//     containerCard.appendChild(img)
//     console.log(personnages[element].src)
// });
function generateUserRow(firstIndex, rowsPage) {
    for (firstIndex; firstIndex < rowsPage; firstIndex++) {
        i++
        element = keys[firstIndex]
        img = document.createElement('img')
        img.classList.add(firstIndex)
        img.src = personnages[element].img
        containerCard.appendChild(img)
        cardImg = document.querySelectorAll("#cardJs img")
    }
}

function nextPage() {
    if (pageActive != numberOfPage) {

        pageActive++
        currentPage.innerHTML = pageActive

        cardImg.forEach(element => {
            // element.innerHTML = ""
            element.remove()
        });

        PageStart += cardsByPage
        PageEnd += cardsByPage
        generateUserRow(PageStart, PageEnd)
    }

}
function previousPage() {
    if (pageActive >= 2) {


        pageActive--
        currentPage.innerHTML = pageActive
        cardImg.forEach(element => {
            // element.innerHTML = ""
            element.remove()
        });
        PageStart = PageStart - cardsByPage
        PageEnd = PageEnd - cardsByPage
        generateUserRow(PageStart, PageEnd)
    }
}

//EVENT LISTENERS
chooseDeck.addEventListener('click', deckConstruct)
nextBtn.addEventListener('click', nextPage);
prevBtn.addEventListener('click', previousPage);

function deckConstruct() {

    deckConstructor = document.createElement("div")
    todolistInput = document.createElement("input")
    btnExit = document.createElement("button")
    killAll = document.createElement("button")
    todolistUl = document.createElement("ul")

    todolistInput.placeholder = "Appuer sur Entrer pour valider une todo"
    btnExit.innerHTML = "Close"

    deckConstructor.classList.add("deckConstructor")
    btnExit.classList.add("btnExit")
    killAll.classList.add("killAll")
    killAll.innerHTML = "effacer toute les taches accomplis"
    
    containerLocation.appendChild(deckConstructor)
    deckConstructor.appendChild(todolistInput)
    deckConstructor.appendChild(btnExit)
    deckConstructor.appendChild(killAll)
    deckConstructor.appendChild(todolistUl)



    todolistInput.addEventListener('keypress', todolistGenerator)
    btnExit.addEventListener('click', closeDeckConstructor)
    todolistUl.addEventListener('click', deleteList)
    killAll.addEventListener('click', deleteAllList)
    todolistUl.addEventListener('click', strikeString)

    paginationCreate()
}



function closeDeckConstructor(params) {
    params.srcElement.closest("div").remove()
}

let maxTodo = 20;
let numberOfTodo = 0 

function todolistGenerator(e) {
    if (e.key === 'Enter' && todolistInput.value != "" && numberOfTodo < maxTodo || e.pointerType == "mouse" && todolistInput.value != "" && numberOfTodo<maxTodo) {
        todolist = document.createElement("li")
        btnDelete = document.createElement("button")
        todolist.innerHTML = todolistInput.value
        todolistInput.value = ""
        btnDelete.innerHTML = "Delete"
        todolistUl.appendChild(todolist)
        todolist.appendChild(btnDelete)
        numberOfTodo++

        actualElementDisplay++
        paginationChange()
    } else if (numberOfTodo >= maxTodo) {
        alert ("Trop de todo merci de supprimer")
    }
}


function deleteList(params) {
    todolistUl.querySelectorAll('button').forEach(element => {
        if (params.srcElement == element) {
            params.srcElement.closest("li").remove()
            numberOfTodo--
            actualElementDisplay--
            paginationChange()
        }
    });
}

function deleteAllList(params) {
    todolistUl.querySelectorAll('strike').forEach(element => {
            element.closest("li").remove()
            numberOfTodo--
            actualElementDisplay--
            paginationChange()
    });
}

function strikeString(params) {
    todolistUl.querySelectorAll('li').forEach(element => {
        if (params.srcElement == element) {
            target = params.srcElement.innerHTML
            params.srcElement.innerHTML = target.strike()
        }
    });
}




//TEST PAGINATION

let actualElementDisplay = 0
let elementByPage = 2 //18
let numberOfPageD = document.createElement('div')
numberOfPageD.innerHTML = "1"

function paginationCreate() {

    let numberOfPageTodolist = todolistUl / elementByPage
    pageActive = 1
    divPagination = document.createElement('div')
    deckConstructor.appendChild(divPagination)
let firstBtnD = firstBtn.cloneNode(true)
let prevBtnD =  prevBtn.cloneNode(true)
let currentPageD = actualPage.cloneNode(true)
let nextBtnD = nextBtn.cloneNode(true)
let lastBtnD = lastBtn.cloneNode(true)

    divPagination.appendChild(firstBtnD)
    divPagination.appendChild(prevBtnD)
    divPagination.appendChild(currentPageD)
    divPagination.appendChild(numberOfPageD)

    divPagination.appendChild(nextBtnD)
    divPagination.appendChild(lastBtnD)
}

function paginationChange() {


    if (actualElementDisplay >= elementByPage ) {
        numberOfPageD.innerHTML++
        actualElementDisplay = 0

        for (let index = 0; index < todolist.length; index++) {
            const element = todolist[index];
            console.log(todolist)
            if (index == 1 ) {
            }
        }
    }
}